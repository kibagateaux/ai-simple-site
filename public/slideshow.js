// selecting the slide section in html
const slideArea = document.querySelector("div.slides"); 
// selecting the images in html
const images = slideArea.querySelectorAll("img");
// selecting the underlines (not visibile until you hover over the titles) in html
const underline = document.querySelectorAll("hr");
// selecting the title text in html
const titles = document.querySelectorAll(".slidetitles");

// we want to keep track of the current title
let currentSlide = 0;



// for each title, do the following
titles.forEach((link, index) => {
  // listen for when the mouse hover over the title
  link.addEventListener("mouseover", function() {
    // index is the number within the link to match with the slides
    // set the current index to the currentSlide
    currentSlide = index;

    // for each image, do the following
    images.forEach((image, index) => {
      // remove all the position classes that were previously applied
      image.classList.remove(
        "position0", "position1", "position2", "position3", "position4", "default0", "default1", "default2", "default3", "default4"
      );
      
      // add the position class to each image in order, so they look stacked in order
      image.classList.add("position" + ((currentSlide + index) % images.length));
    });

    console.log(link, index);

    // remove the animation from the style for every image
    images.forEach(image => {
      image.style.animation = "";
    });

    // remove the underline class for every title
    underline.forEach(line => {
      line.classList.remove("hrhover");
    });

    // add underline animation as you hover
    underline[currentSlide].classList.add("hrhover");

    // adding the fade animation to the image
    console.log("hey");
    images[currentSlide].style.animation = "fade 0.5s";
  });
});

// THAT IS IT 🌟