// Generic node.js express init:
const express = require('express');
const app = express();
const request = require("request");

const blog = require('./blog.json');

// ------------------------------- Express Handlebars is setup here
const jsesc = require('jsesc');
const hbs = require('express-handlebars')({
  defaultLayout: 'main',
  extname: '.hbs',
  helpers: {
    static(path) {
      return path;
    },
    escapeJSString(str) {
      if (! str) {
        return null;
      }
      return jsesc(str, {
        escapeEverything: true, // escape everything to \xFF hex sequences so we don't have to worry about script tags and whatnot
        wrap: true // wrap output with single quotes
      });
    }
  }
});

app.use(express.static('public'));
app.engine('hbs', hbs);
app.set('view engine', 'hbs');


// ----------------------------------------------------------------
// ------------------------------ Your app routes go here
app.get("/", (request, response) => {

  response.render('index', {
    exampleVariable: "Hello world"
  });
});

app.get("/about", (request, response) => {
  
  response.render('about', {
    
  });
});


app.get("/blog/:title", (request, response) => {
  // get blog content with title as key in JSON
  const blogData = blog[request.params.title]
  if(blogData)
    response.render('blog', blogData);
  else
    response.render('404', {toLink: "/", toText: "Return Home"})
  
});

app.get("/project", (request, response) => {
  
  response.render('project', {
    
  });
});

// app.get('/about',function(req,res) {
//     res.sendFile(path.join(__dirname+'/views/about.html'));
// });


// And we end with some more generic node stuff -- listening for requests :-)
let listener = app.listen(process.env.PORT, () => {
  console.log('Your app is listening on port ' + listener.address().port);
});
